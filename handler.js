async function formTable() {
  if (document.getElementById("start").value === "") {
    alert("Вы не ввели начальную дату!");
    return;
  }
  if (document.getElementById("end").value === "") {
    alert("Вы не ввели конечную дату!");
    return;
  }

  const start = new Date(document.getElementById("start").value);
  const end = new Date(document.getElementById("end").value);

  console.log(start);
  console.log(end);

  // сначала отфильтруем по дате
  var date_filtered = [];
  for (var i = 0; i < works.length; i++) {
    var workStart = new Date(works[i].date_start);
    var workEnd = new Date(works[i].date_stop);
    // если наработка входит в промежуток, то закинём в отфильтрованный массив
    if (workEnd >= start && workStart <= end.addDays(1)) {
      date_filtered.push(works[i]);
    }
  }

  if (date_filtered.length === 0) {
    alert("За указанный отчётный период нет наработок!");
    return;
  }

  console.log("Отфильтровано по дате:");
  console.log(date_filtered);

  // потом сформируем массив с уникальными Юзер-Задача
  var unique = {};
  var unique_ghost = [];
  for (var i = 0; i < date_filtered.length; i++) {
    var temp = [];
    // формируем ключ
    temp.push(`${date_filtered[i].user_id}-${date_filtered[i].task_id}`);
    // если ключа нет, то нужно занести
    if (!JSON.stringify(unique_ghost).includes(JSON.stringify(temp))) {
      unique_ghost.push(temp);
      unique[temp] = date_filtered[i];
    }
    // если ключ есть, то по этому ключу просуммировать минуты и секунды
    else {
      console.log("rep");
      unique[temp].seconds += date_filtered[i].seconds;
      unique[temp].minutes += date_filtered[i].minutes;
      unique[temp].hours += date_filtered[i].hours;
    }
  }
  console.log("Уникальный ключ-задача:");
  console.log(unique);

  var data = [];
  for (var key in unique) {
    var temp = [];
    temp.push(
      unique[key].user_name,
      unique[key].project_name,
      unique[key].task_title,
      `${unique[key].hours} часов ${unique[key].minutes} минут ${unique[key].seconds} секунд`
    );
    data.push(temp);
  }
  console.log(data);

  await $("#toExport").DataTable({
    language: {
      url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/ru.json",
    },
    data: data,
  });

  $(".container").removeClass("invisible");
}
