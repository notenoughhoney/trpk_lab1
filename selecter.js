// получение пользователей
const HttpUsers = new XMLHttpRequest();
let url =
  "https://b24-bhbkaj.bitrix24.ru/rest/1/0ud7yg8tltmbznn5/user.get.json?ACTIVE=true";
HttpUsers.open("GET", url, false);
HttpUsers.send();

var users = [];

var obj = JSON.parse(HttpUsers.responseText);
// сфомируем "словарь"
var user = [];
for (var i = 0; i < obj.result.length; i++) {
  user = {};
  user.id = obj.result[i].ID;
  if (obj.result[i].SECOND_NAME != null) {
    user.name = `${obj.result[i].LAST_NAME} ${obj.result[i].NAME} ${obj.result[i].SECOND_NAME}`;
  } else {
    user.name = `${obj.result[i].LAST_NAME} ${obj.result[i].NAME}`;
  }
  users.push(user);
}
console.log("Все прилетевшие пользователи Bitrix:");
console.log(users);

// получение проектов
const HttpProjects = new XMLHttpRequest();
url =
  "https://b24-bhbkaj.bitrix24.ru/rest/1/rqempzede81ym5cb/sonet_group.get.json";
HttpProjects.open("GET", url, false);
HttpProjects.send();

var projects = [];

var obj = JSON.parse(HttpProjects.responseText);
// сфомируем "словарь"
var project = [];
for (var i = 0; i < obj.result.length; i++) {
  project = {};
  project.id = obj.result[i].ID;
  project.name = obj.result[i].NAME;
  projects.push(project);
}
console.log("Все прилетевшие проекты Bitrix:");
console.log(projects);

// получение задач
const HttpTasks = new XMLHttpRequest();
url =
  "https://b24-bhbkaj.bitrix24.ru/rest/1/g5e7x19iate6g7i3/task.item.list.json";
HttpTasks.open("GET", url, false);
HttpTasks.send();

var tasks = [];

var obj = JSON.parse(HttpTasks.responseText);
// сфомируем "словарь"
var task = [];
for (var i = 0; i < obj.result.length; i++) {
  task = {};
  task.id = obj.result[i].ID;
  task.title = obj.result[i].TITLE;
  task.project_id = obj.result[i].GROUP_ID;
  tasks.push(task);
}
console.log("Все прилетевшие задачи Bitrix:");
console.log(tasks);

// получение наработок
const HttpWorks = new XMLHttpRequest();
url =
  "https://b24-bhbkaj.bitrix24.ru/rest/1/evbldr6p6xhmkd3s/task.elapseditem.getlist.json";
HttpWorks.open("GET", url, false);
HttpWorks.send();

var works = [];

var obj = JSON.parse(HttpWorks.responseText);
// сфомируем "словарь"
var work = {};
for (var i = 0; i < obj.result.length; i++) {
  work = {};
  work.id = obj.result[i].ID;
  work.task_id = obj.result[i].TASK_ID;
  work.user_id = obj.result[i].USER_ID;
  work.seconds = parseInt((obj.result[i].SECONDS >= 60) ? obj.result[i].SECONDS % 60 : obj.result[i].SECONDS);
  work.minutes = parseInt((obj.result[i].MINUTES >= 60) ? obj.result[i].MINUTES % 60 : obj.result[i].MINUTES);
  work.hours = parseInt((obj.result[i].MINUTES / 60) | 0);
  work.date_start = obj.result[i].DATE_START;
  work.date_stop = obj.result[i].DATE_STOP;
  works.push(work);
}
console.log("Все прилетевшие наработки:");
console.log(works);


// прицепим пользователей
for (var i = 0; i < works.length; i++) {
  works[i].user_name = getValueByKey(users, works[i].user_id, "id", "name");
}
console.log("Прицепили пользователей:");
console.log(works);

// прицепим задачи c индексами проектов
for (var i = 0; i < works.length; i++) {
  works[i].task_title = getValueByKey(tasks, works[i].task_id, "id", "title");
  works[i].project_id = getValueByKey(tasks, works[i].task_id, "id", "project_id");
}
console.log("Прицепили задачи c индексами проектов:");
console.log(works);

// прицепим названия проектов
for (var i = 0; i < works.length; i++) {
  works[i].project_name = getValueByKey(projects, works[i].project_id, "id", "name");
}
console.log("Прицепили названия проектов:");
console.log(works);