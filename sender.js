function send() {
  // создаём рабочий лист
  const worksheet = XLSX.utils.table_to_sheet(document.getElementById("toExport"));
  // устанавливаем ширину для колонок
  worksheet["!cols"] = [{wch: 26}, {wch: 23}, {wch: 40}, {wch: 25},];
  // создаём рабочую книгу
  const workbook = XLSX.utils.book_new();
  // добавляем на неё рабочий лист
  XLSX.utils.book_append_sheet(workbook, worksheet, "Отчёт");

  // в бинарную строку
  var xlsbin = XLSX.write(workbook, {
    bookType: "xlsx",
    type: "binary",
  });

  // в BLOB
  var buffer = new ArrayBuffer(xlsbin.length),
    array = new Uint8Array(buffer);
  for (var i = 0; i < xlsbin.length; i++) {
    array[i] = xlsbin.charCodeAt(i) & 0xff;
  }
  var xlsblob = new Blob([buffer], { type: "application/octet-stream" });
  delete array;
  delete buffer;
  delete xlsbin;

  // получаем текущую дату и время, чтобы проименовать отчёт
  var currentdate = new Date();
  var datetime =
    currentdate.getDate() +
    "-" +
    (currentdate.getMonth() + 1) +
    "-" +
    currentdate.getFullYear() +
    "__" +
    currentdate.getHours() +
    "-" +
    currentdate.getMinutes() +
    "-" +
    currentdate.getSeconds();

  // здесь просим URL для заливки файла
  const HttpGetU = new XMLHttpRequest();
  let url = `https://cloud-api.yandex.net/v1/disk/resources/upload?path=/report_${datetime}.xlsx`;
  HttpUsers.open("GET", url, false);
  HttpUsers.setRequestHeader(
    "Authorization",
    "OAuth y0_AgAEA7qkROu2AAh1DgAAAADQEW1E2jYy3NdeQjmTfoVA4tlW8H51OSg"
  );
  HttpUsers.send();
  var obj = JSON.parse(HttpUsers.responseText);

  // здесь заливаем файл
  const upload = (file) => {
    fetch(obj.href, {
      method: "PUT",
      body: file, // объект файла
    })
      .then(
        (response) => {
          console.log(response);
          if (response.status === 201) Swal.fire({
            icon: 'success',
            title: 'Успешно!',
            text: 'Отчёт доступен по адресу внизу',
            footer: `<a href="https://disk.yandex.ru/edit/disk/disk%2Freport_${datetime}.xlsx">Перейти...</a>`
          })
        } // if the response is a JSON object
      )
      .then(
        (success) => console.log(success) // Handle the success response object
      )
      .catch(
        (error) => console.log(error) // Handle the error response object
      );
  };

  upload(xlsblob);
}

$("#toExport").on("draw.dt", function () {
  send();
});
