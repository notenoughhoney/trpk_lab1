// mas - из какого массива
// v   - значение ключа
// key - ключ
// wh  - по какому ключу возвратить
function getValueByKey(mas, v, key, wh) {
  // проход по каждому объекту из переданного массива
  for (var i = 0; i < mas.length; i++) {
    if (v === mas[i][key]) return mas[i][wh];
  }
}

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}
